# Generated by Django 3.0.3 on 2020-03-26 20:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recruiter', '0009_auto_20200327_0233'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recruiter',
            old_name='personal_number',
            new_name='slug',
        ),
    ]
