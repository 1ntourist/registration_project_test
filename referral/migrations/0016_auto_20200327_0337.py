# Generated by Django 3.0.3 on 2020-03-26 21:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('referral', '0015_auto_20200327_0336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referral',
            name='status',
            field=models.CharField(choices=[('sure', 'Нет совпадений'), ('unsure', 'Есть совпадения')], default='Нет совпадений', max_length=20),
        ),
    ]
