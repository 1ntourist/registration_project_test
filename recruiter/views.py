from django.views.generic import CreateView, TemplateView, ListView, DetailView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from .models import Recruiter
from .forms import RecruiterCreateForm


class RecruiterListView(LoginRequiredMixin, ListView):
    model = Recruiter
    template_name = 'list_of_recruiter.html'
    paginate_by = 3
    context_object_name = 'recruiters'
    ordering = ('last_name', 'first_name', 'middle_name',)


class RecruiterDetailView(LoginRequiredMixin, DetailView):
    model = Recruiter
    template_name = 'detail_of_recruiter.html'
    slug_field = 'slug'


class RecruiterCreateView(LoginRequiredMixin, CreateView):
    model = Recruiter
    form_class = RecruiterCreateForm
    template_name = 'create_of_recruiter.html'
    slug_field = 'slug'


class RecruiterUpdateView(LoginRequiredMixin, UpdateView):
    model = Recruiter
    form_class = RecruiterCreateForm
    template_name = 'update_of_recruiter.html'
    slug_field = 'slug'


class RecruiterDeleteView(LoginRequiredMixin, DeleteView):
    model = Recruiter
    template_name = 'delete_of_recruiter.html'
    success_url = reverse_lazy('recruiter:list')
    slug_field = 'slug'
