from django.db import models
from django.urls import reverse
from django.utils.datetime_safe import datetime

from recruiter.models import Recruiter

STATUS_CHOICES = (
    ('Нет совпадений', 'Нет совпадений'), ('Есть совпадения', 'Есть совпадения'),
)


class Referral(models.Model):
    last_name = models.CharField(max_length=50, db_index=True)
    first_name = models.CharField(max_length=50, db_index=True)
    middle_name = models.CharField(max_length=50, blank=True, null=True)
    passport_id = models.CharField(max_length=9, db_index=True, unique=True)
    slug = models.SlugField(max_length=14, unique=True)
    area = models.CharField(max_length=50)
    region = models.CharField(max_length=50)
    village = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=50)
    birthday = models.DateField(default=datetime.now)
    recruiter = models.ForeignKey(Recruiter, on_delete=models.CASCADE,  verbose_name='Рекрутер', related_name='recruiter')
    status = models.CharField(choices=STATUS_CHOICES, max_length=20, default='Нет совпадений')

    def get_absolute_url(self):
        return reverse('referral:detail', args=[self.slug])
