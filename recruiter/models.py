from django.db import models
from django.urls import reverse
from django.utils.datetime_safe import datetime


class Recruiter(models.Model):
    last_name = models.CharField(max_length=50, db_index=True)
    first_name = models.CharField(max_length=50, db_index=True)
    middle_name = models.CharField(max_length=50, blank=True, null=True)
    passport_id = models.CharField(max_length=9, db_index=True, unique=True)
    slug = models.SlugField(max_length=14, unique=True)
    area = models.CharField(max_length=50)
    region = models.CharField(max_length=50)
    village = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=50)
    birthday = models.DateField(default=datetime.now)

    def __str__(self):
        return '{} {} {}'.format(self.last_name, self.first_name, self.middle_name)

    def get_absolute_url(self):
        return reverse('recruiter:detail', args=[self.slug])
