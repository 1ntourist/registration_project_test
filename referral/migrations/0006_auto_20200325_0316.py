# Generated by Django 3.0.3 on 2020-03-24 21:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recruiter', '0006_auto_20200325_0316'),
        ('referral', '0005_auto_20200325_0303'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='referral',
            name='recruiter',
        ),
        migrations.AddField(
            model_name='referral',
            name='recruiter',
            field=models.ManyToManyField(to='recruiter.Recruiter', verbose_name='Рекрутер'),
        ),
    ]
